<?php

namespace mindplay\sql;

use mindplay\sql\facets\Converter;
use mindplay\sql\model\Context;
use mindplay\sql\model\Query;
use mindplay\sql\schema\Column;
use mindplay\sql\schema\Table;
use RuntimeException;

/**
 * This class represents an INSERT query.
 */
class InsertQuery extends Query
{
    /**
     * @var Table
     */
    private $table;

    /**
     * @var mixed[][] list of record maps, where Column name => value
     */
    private $records = [];

    /**
     * @param Table                  $into   Table to INSERT into
     * @param mixed[]|mixed[][]|null $record optional record map (or list of record maps) where Column name => value
     */
    public function __construct(Table $into, array $record = null)
    {
        $this->table = $into;

        if ($record !== null) {
            $this->add($record);
        }
    }

    /**
     * Add one or more records to this INSERT query.
     *
     * @param mixed[]|mixed[][] $record record map (or list of record maps) where Column name => value
     */
    public function add(array $record)
    {
        reset($record);

        $first_key = key($record);

        if ($first_key === null) {
            return; // empty array given - no records added
        }

        if (is_int($first_key)) {
            // append given list of records:
            $this->records = array_merge($this->records, $record);
        } else {
            // append given single record:
            $this->records[] = $record;
        }
    }

    public function buildQuery(Context $context)
    {
        if (count($this->records) === 0) {
            throw new RuntimeException("no records added to this query");
        }

        $table = $context->driver->quoteName($this->table->getName());

        $columns = array_filter(
            $this->table->listColumns(),
            function (Column $column) {
                return $column->auto === false; // filter auto-generated Columns
            }
        );

        $quoted_column_names = implode(
            ", ",
            array_map(
                function (Column $column) use ($context) {
                    return $context->driver->quoteName($column->name);
                },
                $columns
            )
        );

        $tuples = [];

        $converters = $this->createConverters();

        $param_index = 0;

        foreach ($this->records as $tuple_num => $record) {
            $placeholders = [];

            foreach ($columns as $name => $column) {
                if (array_key_exists($name, $record)) {
                    $php_value = $record[$name];
                } elseif ($column->required === false) {
                    $php_value = $column->default;
                } else {
                    throw new RuntimeException("required value \"{$column->name}\" missing from tuple #{$tuple_num}");
                }

                $sql_value = isset($converters[$name])
                    ? $converters[$name]->convertToSQL($php_value)
                    : $php_value; // no conversion defined

                $placeholders[] = $context->params->bind($param_index, $sql_value);

                $param_index += 1;
            }

            $tuples[] = "(" . implode(", ", $placeholders) . ")";
        }

        return "INSERT INTO {$table} ({$quoted_column_names}) VALUES\n" . implode(",\n", $tuples);
    }

    /**
     * @return Converter[] map where Column name => Converter instance
     */
    protected function createConverters()
    {
        $columns = $this->table->listColumns();

        $converters = [];

        foreach ($columns as $column) {
            if ($column->type instanceof Converter) {
                $converters[$column->name] = $column->type;
            }
        }

        return $converters;
    }
}
