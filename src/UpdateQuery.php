<?php

namespace mindplay\sql;

use mindplay\sql\facets\Converter;
use mindplay\sql\facets\Expression;
use mindplay\sql\model\Context;
use mindplay\sql\model\ProjectionQuery;
use mindplay\sql\schema\Table;

/**
 * This class represents an UPDATE query.
 */
class UpdateQuery extends ProjectionQuery
{
    /**
     * @var Table
     */
    private $table;

    /**
     * @var mixed[] map where Column name => literal value (or Expression) to assign
     */
    private $assignments = [];

    /**
     * @param Table $table Table to UPDATE
     */
    public function __construct(Table $table)
    {
        parent::__construct($table->alias(null));

        $this->table = $table;
    }

    /**
     * @param string           $column_name name of Column to update
     * @param mixed|Expression $value       literal value, or Expression to assign
     */
    public function set($column_name, $value)
    {
        $this->assignments[$column_name] = $value;
    }

    /**
     * @param array $values map where Column name => literal values (or Expression instances) to assign
     */
    public function assign(array $values)
    {
        $this->assignments = array_merge($this->assignments, $values);
    }

    public function buildQuery(Context $context)
    {
        $update = "UPDATE " . $this->buildNodes($context);

        $set = "\nSET " . implode("\n,  ", $this->buildAssignments($context));

        $where = count($this->where->conds)
            ? "\nWHERE " . $this->buildConditions($context)
            : ''; // no conditions present

        return "{$update}{$set}{$where}";
    }

    /**
     * @param Context $context
     *
     * @return string[] column value assignments for use in the SET clause
     */
    protected function buildAssignments(Context $context)
    {
        $assignments = [];

        foreach ($this->assignments as $column_name => $value) {
            $column = $this->table->getColumn($column_name);

            if ($value instanceof Expression) {
                $sql_expr = $value->buildExpression($context, $column_name);
            } else {
                $php_value = $value;

                if ($column->type instanceof Converter) {
                    $sql_value = $column->type->convertToSQL($php_value);
                } else {
                    $sql_value = $php_value;
                }

                $sql_expr = $context->params->bind("new_{$column_name}", $sql_value);
            }

            $assignments[] = $context->driver->quoteName($column_name) . ' = ' . $sql_expr;
        }

        return $assignments;
    }
}
