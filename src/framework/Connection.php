<?php

namespace mindplay\sql\framework;

use Exception;
use InvalidArgumentException;
use LogicException;
use mindplay\sql\facets\MapperFactory;
use mindplay\sql\model\Context;
use mindplay\sql\model\Query;
use mindplay\sql\schema\Database;
use mindplay\sql\SelectQuery;
use PDO;
use UnexpectedValueException;

/**
 * This class represents a PDO Connection to a Database using a Driver.
 */
class Connection
{
    /**
     * @var Driver
     */
    public $driver;

    /**
     * @var PDO
     */
    public $pdo;

    /**
     * @var int default batch-size when processing large result sets
     *
     * @see fetch()
     */
    public $default_batch_size = 100;

    /**
     * @var int number of nested calls to transact()
     *
     * @see transact()
     */
    private $transaction_level = 0;

    /**
     * @var bool net result of nested calls to transact()
     *
     * @see transact()
     */
    private $transaction_result;

    /**
     * @param Driver   $driver
     * @param PDO      $pdo
     */
    public function __construct(Driver $driver, PDO $pdo)
    {
        $this->driver = $driver;
        $this->pdo = $pdo;
    }

    /**
     * @param SelectQuery $query
     * @param int|null    $batch_size batch-size when processing large result sets
     *
     * @return Statement
     */
    public function fetch(SelectQuery $query, $batch_size = null)
    {
        return $this->prepare($query, $batch_size ?: $this->default_batch_size);
    }

    /**
     * @param Query $query
     *
     * @return void
     */
    public function exec(Query $query)
    {
        $statement = $this->prepare($query);

        $statement->exec();
    }

    /**
     * @param Query    $query
     * @param int|null $batch_size batch-size when processing large result sets
     *
     * @return Statement
     */
    public function prepare(Query $query, $batch_size = null)
    {
        $context = new Context($this->driver);

        $sql = $query->buildQuery($context);

        $handle = $this->pdo->prepare($sql);

        $vars = $context->params->getVars();

        $mappers = $query instanceof MapperFactory
            ? $query->createMappers()
            : [];

        return new Statement($handle, $vars, $mappers, $batch_size ?: $this->default_batch_size);
    }

    /**
     * @param callable $func function () : bool - must return TRUE to commit or FALSE to roll back
     *
     * @return bool TRUE on success (committed) or FALSE on failure (rolled back)
     *
     * @throws InvalidArgumentException if the provided argument is not a callable function
     * @throws LogicException if an unhandled Exception occurs while calling the provided function
     * @throws UnexpectedValueException if the provided function does not return TRUE or FALSE
     */
    public function transact($func)
    {
        if (! is_callable($func)) {
            throw new InvalidArgumentException("invalid argument: \$func is not callable");
        }

        if ($this->transaction_level === 0) {
            // starting a new stack of transactions - assume success:
            $this->pdo->beginTransaction();
            $this->transaction_result = true;
        }

        $this->transaction_level += 1;

        /** @var mixed $commit return type of $func isn't guaranteed, therefore mixed rather than bool */

        try {
            $commit = call_user_func($func);
        } catch (Exception $exception) {
            $commit = false;
        }

        $this->transaction_result = ($commit === true) && $this->transaction_result;

        $this->transaction_level -= 1;

        if ($this->transaction_level === 0) {
            if ($this->transaction_result === true) {
                $this->pdo->commit();

                return true; // the net transaction is a success!
            } else {
                $this->pdo->rollBack();
            }
        }

        if (isset($exception)) {
            // re-throw unhandled Exception as a LogicException:
            throw new LogicException("unhandled Exception during transaction", 0, $exception);
        }

        if (! is_bool($commit)) {
            throw new UnexpectedValueException("\$func must return TRUE (to commit) or FALSE (to roll back)");
        }

        return $this->transaction_result;
    }
}
