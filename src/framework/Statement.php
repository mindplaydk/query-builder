<?php

namespace mindplay\sql\framework;

use ArrayAccess;
use Iterator;
use IteratorAggregate;
use mindplay\sql\facets\Mapper;
use PDO;
use PDOStatement;
use RuntimeException;

/**
 * This class represents a prepared statement and bound parameters.
 *
 * It implements `IteratorAggregate`, allowing you to execute the query and iterate
 * over the results using a `foreach` statement.
 *
 * It implements `ArrayAccess`, allowing you to use array-syntax to read/write any
 * parameters bound to the underlying statement.
 */
class Statement implements IteratorAggregate, ArrayAccess
{
    /**
     * @var PDOStatement
     */
    public $handle;

    /**
     * @var array bound parameters
     */
    protected $params = [];

    /**
     * @var Mapper[] list of Mappers to apply when fetching results
     */
    protected $mappers;

    /**
     * @var int batch-size when processing large result sets
     */
    protected $batch_size;

    /**
     * @param PDOStatement $handle PDO statement handle
     * @param array        $params parameters to bind
     * @param Mapper[]     $mappers list of Mappers to apply when fetching results
     */
    public function __construct(PDOStatement $handle, array $params, array $mappers = [], $batch_size = 100)
    {
        $this->handle = $handle;
        $this->mappers = $mappers;
        $this->batch_size = $batch_size;

        $this->apply($params);
    }

    /**
     * Execute the prepared SQL statement
     *
     * @return PDOStatement
     *
     * @throws RuntimeException if unable to execute SQL statement
     */
    public function exec()
    {
        if ($this->handle->execute() === false) {
            $info = $this->handle->errorInfo();

            $sql = $this->handle->queryString;

            throw new RuntimeException(
                "unable to execute SQL statement"
                . "\nQUERY:  {$sql}"
                . "\nERROR:  " . implode("; ", $info)
                . "\nPARAMS: " . var_export($this->params, true)
                . "\nBOUND:  " . $this->emulatedPrepare($sql, $this->params)
            );
        }

        return $this->handle;
    }

    /**
     * @return mixed|null first record of the record-set (or NULL, if there are no records in the record-set)
     */
    public function firstRow()
    {
        foreach ($this->createIterator(1) as $record) {
            return $record; // break from loop immediately after fetching the first record
        }

        return null;
    }

    /**
     * @return mixed|null first column value of the first record of the record-set (or NULL, if the record-set is empty)
     */
    public function firstCol()
    {
        foreach ($this->createIterator(1) as $record) {
            $keys = array_keys($record);

            return $record[$keys[0]]; // break from loop immediately after fetching the first record
        }

        return null;
    }

    /**
     * Execute this Statement and return a Generator, so you can iterate over the results.
     * 
     * This method implements `IteratorAggregate`, permitting you to iterate directly over
     * a `Statement` instance without explicitly calling this method.
     * 
     * @return Iterator
     */
    public function getIterator()
    {
        return $this->createIterator($this->batch_size);
    }

    /**
     * Apply (bind) a map of parameter name/value pairs, overriding any defined parameters
     * with the same names.
     *
     * @param array $params
     *
     * @return void
     */
    public function apply(array $params)
    {
        foreach ($params as $name => $value) {
            $this->offsetSet($name, $value);
        }
    }

    /**
     * @return array bound parameters
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @ignore
     */
    public function offsetExists($offset)
    {
        return isset($this->params[$offset]);
    }

    /**
     * @ignore
     */
    public function offsetGet($offset)
    {
        return isset($this->params[$offset])
            ? $this->params[$offset]
            : null;
    }

    /**
     * @ignore
     */
    public function offsetSet($offset, $value)
    {
        $this->params[$offset] = $value;

        $this->handle->bindValue($offset, $value);
    }

    /**
     * @ignore
     */
    public function offsetUnset($offset)
    {
        unset($this->params[$offset]);

        $this->handle->bindValue($offset, null);
    }

    /**
     * Create an Iterator with a given batch-size.
     *
     * @param int $batch_size batch-size when processing the result set
     *
     * @return Iterator
     */
    protected function createIterator($batch_size)
    {
        $statement = $this->exec();

        $fetching = true;

        do {
            // fetch a batch of records:

            $batch = [];

            do {
                $record = $statement->fetch(PDO::FETCH_ASSOC);

                if ($record === false) {
                    if (count($batch) === 0) {
                        return; // last batch of records fetched
                    }

                    $fetching = false; // last record of batch fetched
                } else {
                    $batch[] = $record;
                }
            } while ($fetching && (count($batch) < $batch_size));

            // apply Mappers to current batch of records:

            $num_records = count($batch);

            foreach ($this->mappers as $index => $mapper) {
                $batch = $mapper->map($batch);

                if (count($batch) !== $num_records) {
                    $count = count($batch);

                    throw new RuntimeException("Mapper #{$index} returned {$count} records, expected: {$num_records}");
                }
            }

            // return each record from the current batch:

            foreach ($batch as $record) {
                yield $record;
            }
        } while ($fetching);
    }

    /**
     * @param string $sql
     * @param array  $params
     *
     * @return string SQL with emulated prepare (for diagnostic purposes only)
     */
    private function emulatedPrepare($sql, array $params)
    {
        foreach ($params as $name => $value) {
            $quoted_value =
                $value === null
                    ? "NULL"
                    : (is_numeric($value) ? $value : "\"{$value}\"");

            $sql = str_replace(":{$name}", $quoted_value, $sql);
        }

        return $sql;
    }
}
