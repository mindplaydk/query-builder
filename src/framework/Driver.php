<?php

namespace mindplay\sql\framework;

use mindplay\sql\schema\Database;

/**
 * This class implements a driver model for DBMS-specific operations.
 */
abstract class Driver
{
    /**
     * @param string $name table or column name
     *
     * @return string quoted name
     */
    abstract public function quoteName($name);

    /**
     * Bootstrap the Database with Types etc.
     *
     * @param Database $db
     *
     * @return void
     */
    abstract public function bootstrap(Database $db);
}
