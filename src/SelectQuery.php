<?php

namespace mindplay\sql;

use mindplay\sql\facets\Expression;
use mindplay\sql\facets\ProjectionBuilder;
use mindplay\sql\model\Context;
use mindplay\sql\model\MappedProjectionQuery;
use RangeException;

/**
 * This class represents a SELECT query.
 *
 * It can be built as a stand-alone query, and can also be built as an Expression in
 * the form of a nested SELECT.
 */
class SelectQuery extends MappedProjectionQuery implements Expression
{
    /**
     * @var int|null
     */
    private $offset;

    /**
     * @var int|null
     */
    private $limit;

    /**
     * @param ProjectionBuilder|ProjectionBuilder[] one or more Projections to be added
     */
    public function select($projection_builders)
    {
        $this->addProjectionBuilders($projection_builders);
    }

    /**
     * @param int      $limit  max. number of records
     * @param int|null $offset base-0 record number offset
     *
     * @throws RangeException if the given limit is less than 1, or if the given offset if less than 0
     */
    public function limit($limit, $offset = null)
    {
        if ($limit < 1) {
            throw new RangeException("limit out of range: {$limit}");
        }

        if ($offset < 0) {
            throw new RangeException("offset out of range: {$offset}");
        }

        $this->limit = $limit;
        $this->offset = $offset;
    }

    /**
     * @param int $page_num  base-1 page number
     * @param int $page_size number of records per page
     *
     * @throws RangeException if the given page number or page size are less than 1
     */
    public function page($page_num, $page_size)
    {
        if ($page_size < 1) {
            throw new RangeException("page size out of range: {$page_size}");
        }

        if ($page_num < 1) {
            throw new RangeException("page number out of range: {$page_num}");
        }

        $this->limit($page_size, ($page_num - 1) * $page_size);
    }

    public function buildQuery(Context $context)
    {
        return $this->buildSelectQuery($context);
    }

    public function buildExpression(Context $context, $index)
    {
        return "(" . $this->buildSelectQuery($context, $index) . ")";
    }

    /**
     * @param Context $context
     * @param string  $index
     *
     * @return string SELECT statement template
     */
    protected function buildSelectQuery(Context $context, $index = '')
    {
        $select = "SELECT " . $this->buildProjectionVars($context);

        $from = "\nFROM " . $this->buildNodes($context);

        $where = count($this->where->conds)
            ? "\nWHERE " . $this->buildConditions($context, $index)
            : ''; // no conditions present

        $order = count($this->order)
            ? "\nORDER BY " . $this->buildOrderTerms($context, $index)
            : ''; // no order terms

        $limit = $this->limit !== null
            ? "\nLIMIT {$this->limit}"
                . ($this->offset !== null ? " OFFSET {$this->offset}" : '')
            : ''; // no limit or offset

        return "{$select}{$from}{$where}{$order}{$limit}";
    }
}
