<?php

namespace mindplay\sql\types;

use DateTime;
use DateTimeZone;
use mindplay\sql\facets\Converter;
use mindplay\sql\facets\Type;
use UnexpectedValueException;

/**
 * This class maps an SQL DATETIME value to a Unix timestamp (integer) value in PHP.
 *
 * It assumes DATETIME values being stored relative to the UTC timezone.
 */
class TimestampType implements Type, Converter
{
    const NAME = 'timestamp';

    const DEFAULT_DATETIME_FORMAT = 'Y-m-d H:i:s';

    /**
     * @var string
     */
    private $format;

    /**
     * @param string $format
     */
    public function __construct($format = null)
    {
        $this->format = $format ?: self::DEFAULT_DATETIME_FORMAT;
    }

    /**
     * @return DateTimeZone
     */
    private static function UTC()
    {
        static $utc;

        if ($utc === null) {
            $utc = new DateTimeZone('UTC');
        }

        return $utc;
    }

    public function getName()
    {
        return self::NAME;
    }

    public function convertToSQL($value)
    {
        if ($value === null || $value === '') {
            return null;
        }

        $timestamp = (int) $value;

        if ($timestamp === 0) {
            throw new UnexpectedValueException("unable to convert value to int: " . $value);
        }

        $datetime = DateTime::createFromFormat('U', $timestamp, self::UTC());

        return $datetime->format($this->format);
    }

    public function convertToPHP($value)
    {
        if (is_int($value)) {
            return $value; // return timestamp as-is
        }

        if ($value === null) {
            return $value; // return NULL value as-is
        }

        $datetime = DateTime::createFromFormat($this->format, $value, self::UTC());

        if ($datetime === false) {
            throw new UnexpectedValueException("unable to convert value from int: " . $value);
        }

        return $datetime->getTimestamp();
    }
}
