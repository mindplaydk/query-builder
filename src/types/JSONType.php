<?php

namespace mindplay\sql\types;

use mindplay\sql\facets\Converter;
use mindplay\sql\facets\Type;

class JSONType implements Type, Converter
{
    const NAME = 'json';

    public function getName()
    {
        return self::NAME;
    }

    public function convertToSQL($value)
    {
        if ($value === null || $value === '') {
            return null;
        }

        return json_encode($value);
    }

    public function convertToPHP($value)
    {
        return ($value === null || $value === '')
            ? null
            : json_decode($value, true);
    }
}
