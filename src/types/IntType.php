<?php

namespace mindplay\sql\types;

use mindplay\sql\facets\Converter;
use mindplay\sql\facets\Type;

class IntType implements Type, Converter
{
    const NAME = 'int';

    public function getName()
    {
        return self::NAME;
    }

    public function convertToSQL($value)
    {
        return $value;
    }

    public function convertToPHP($value)
    {
        return $value === null
            ? null
            : (int) $value;
    }
}
