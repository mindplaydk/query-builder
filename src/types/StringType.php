<?php

namespace mindplay\sql\types;

use mindplay\sql\facets\Type;

class StringType implements Type
{
    const NAME = 'string';

    public function getName()
    {
        return self::NAME;
    }
}
