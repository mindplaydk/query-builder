<?php

namespace mindplay\sql\model;

use mindplay\sql\facets\Expression;

class LeftJoin extends Join
{
    /**
     * @param Node       $node
     * @param Expression $condition
     */
    public function __construct(Node $node, Expression $condition)
    {
        parent::__construct(self::LEFT, $node, $condition);
    }
}
