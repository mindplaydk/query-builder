<?php

namespace mindplay\sql\model;

use mindplay\sql\framework\Driver;

/**
 * This model represents a Context in which a Query is being built.
 */
class Context
{
    /**
     * @var Driver
     */
    public $driver;

    /**
     * @var Params
     */
    public $params;

    public function __construct(Driver $driver)
    {
        $this->driver = $driver;
        $this->params = new Params();
    }
}
