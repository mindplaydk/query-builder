<?php

namespace mindplay\sql\model;

use mindplay\sql\facets\Reference;

/**
 * This model represents a qualified reference, such a `table`.`column` or `alias`.`column`, etc.
 */
class QualifiedReference implements Reference
{
    /**
     * @var string
     */
    private $table_name;

    /**
     * @var string
     */
    private $column_name;

    /**
     * @param string $table_name
     * @param string $column_name
     */
    public function __construct($table_name, $column_name)
    {
        $this->table_name = $table_name;
        $this->column_name = $column_name;
    }

    public function resolve(Context $context)
    {
        return $context->driver->quoteName($this->table_name) . '.' . $context->driver->quoteName($this->column_name);
    }
}
