<?php

namespace mindplay\sql\model;

use mindplay\sql\facets\Expression;

class RightJoin extends Join
{
    /**
     * @param Node       $node
     * @param Expression $condition
     */
    public function __construct(Node $node, Expression $condition)
    {
        parent::__construct(self::RIGHT, $node, $condition);
    }
}
