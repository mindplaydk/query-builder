<?php

namespace mindplay\sql\model;

use mindplay\sql\facets\Expression;
use mindplay\sql\SelectQuery;

class CountExpression implements Expression
{
    /**
     * @var SelectQuery
     */
    private $select;

    public function __construct(SelectQuery $select)
    {
        $this->select = $select;
    }

    public function buildExpression(Context $context, $index)
    {
        $select = $this->select->buildExpression($context, $index);

        return "COUNT({$select})";
    }
}
