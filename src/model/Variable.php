<?php

namespace mindplay\sql\model;

use mindplay\sql\facets\Expression;
use mindplay\sql\facets\ProjectionBuilder;
use mindplay\sql\facets\Reference;
use mindplay\sql\facets\Type;

/**
 * This class represents an SQL Variable Expression and/or Projection, such as
 * a Column reference, or a projected or computed value to be returned by a Query.
 */
class Variable implements Expression, ProjectionBuilder
{
    /**
     * @var string variable name
     */
    public $name;

    /**
     * @var string|Reference literal initialization or Reference
     */
    public $init;

    /**
     * @var Type
     */
    public $type;

    /**
     * Variable constructor.
     *
     * @param string           $name
     * @param string|Reference $init
     * @param Type             $type
     */
    public function __construct($name, $init, Type $type)
    {
        $this->name = $name;
        $this->init = $init;
        $this->type = $type;
    }

    /**
     * @param Context $context
     *
     * @return string
     */
    private function resolveInit(Context $context)
    {
        return $this->init instanceof Reference
            ? $this->init->resolve($context)
            : (string) $this->init;
    }

    public function buildExpression(Context $context, $index)
    {
        return $this->resolveInit($context);
    }

    public function buildProjectionVar(Context $context)
    {
        return $this->resolveInit($context) . ' AS ' . $context->driver->quoteName($this->name);
    }

    public function buildProjections()
    {
        return [new Projection($this->name, $this->type)];
    }
}
