<?php

namespace mindplay\sql\model;

use mindplay\sql\facets\Expression;
use UnexpectedValueException;

/**
 * This class models a list of Expressions to be combined using `AND` or `OR` operators.
 */
class GroupExpression implements Expression
{
    /**
     * @var string combination operator
     */
    public $operator;

    /**
     * @var Expression[] list of Expressions to be combined
     */
    public $conds = [];

    /**
     * @param string $operator combination operator, e.g. "AND" or "OR"
     */
    public function __construct($operator)
    {
        $this->operator = $operator;
    }

    /**
     * @param Context $context
     * @param string  $index
     *
     * @return string
     *
     * @throws UnexpectedValueException if the condition list is empty
     */
    public function buildExpression(Context $context, $index)
    {
        if (count($this->conds) === 0) {
            throw new UnexpectedValueException("unexpected empty condition list");
        }

        $exprs = [];

        foreach ($this->conds as $subindex => $cond) {
            $exprs[] = $cond->buildExpression($context, $index ? "{$index}_{$subindex}" : $subindex);
        }

        return count($exprs) > 1
            ? "(" . implode(" {$this->operator} ", $exprs) . ")"
            : $exprs[0];
    }
}
