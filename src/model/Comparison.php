<?php

namespace mindplay\sql\model;

use mindplay\sql\facets\Expression;

/**
 * This class models an `X LIKE Y` expression, where `X` and `Y` are either values
 * to be bound to variables (when the expression is built) and `LIKE` is any kind
 * of operator supported by the SQL dialect in use.
 */
class Comparison implements Expression
{
    /**
     * @var Expression|mixed
     */
    public $left;

    /**
     * @var string
     */
    public $operator;

    /**
     * @var Expression|mixed
     */
    public $right;

    /**
     * @param Expression|mixed $left
     * @param string           $operator
     * @param Expression|mixed $right
     */
    public function __construct($left, $operator, $right)
    {
        $this->left = $left;
        $this->operator = $operator;
        $this->right = $right;
    }

    public function buildExpression(Context $context, $index)
    {
        $left = $this->left instanceof Expression
            ? $this->left->buildExpression($context, $index)
            : $context->params->bind($index, $this->left);

        $right = $this->right instanceof Expression
            ? $this->right->buildExpression($context, $index)
            : $context->params->bind($index, $this->right);

        return "{$left} {$this->operator} {$right}";
    }
}
