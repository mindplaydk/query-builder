<?php

namespace mindplay\sql\model;

use mindplay\sql\facets\Expression;

/**
 * This class represents a single term in the ORDER BY clause of an SQL query.
 *
 * @see ProjectionQuery::$order
 */
class Order
{
    const ASC  = 'ASC';
    const DESC = 'DESC';

    /**
     * @var Expression
     */
    public $expression;

    /**
     * @var string|null "ASC" or "DESC"
     */
    public $direction;

    /**
     * @param Expression  $expression
     * @param string|null $direction optional sort order; Order::ASC or Order::DESC; defaults to NULL (ascending order)
     */
    public function __construct(Expression $expression, $direction = null)
    {
        $this->expression = $expression;
        $this->direction = $direction;
    }
}
