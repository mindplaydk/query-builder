<?php

namespace mindplay\sql\model;

use mindplay\sql\facets\Expression;

/**
 * This class represents a JOIN clause.
 */
class Join
{
    const INNER = 'INNER';
    const LEFT = 'LEFT';
    const RIGHT = 'RIGHT';

    /**
     * @var string JOIN type ("INNER", "LEFT" or "RIGHT")
     */
    public $type;

    /**
     * @var Node
     */
    public $table;

    /**
     * @var Expression
     */
    public $on;

    /**
     * @param string     $type JOIN type ("INNER", "LEFT" or "RIGHT")
     * @param Node       $node the Table Node to JOIN with
     * @param Expression $condition JOIN condition Expression
     */
    public function __construct($type, Node $node, Expression $condition)
    {
        $this->type = $type;
        $this->table = $node;
        $this->on = $condition;
    }

    /**
     * @param Context $context query context
     *
     * @return string SQL JOIN clause
     */
    public function buildJoin(Context $context)
    {
        $expr = $this->on->buildExpression($context, '');

        return "{$this->type} JOIN " . $this->table->buildNode($context). " ON {$expr}";
    }
}
