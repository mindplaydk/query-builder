<?php

namespace mindplay\sql\model;

/**
 * This class represents a map of placeholders and values, with the ability to
 * bind one or multiple values at a time.
 */
class Params
{
    /**
     * @var mixed[] map where variable name => scalar value
     */
    protected $vars = [];

    /**
     * Bind a given value, or list of values, to a placeholder with a given index, or
     * to multiple placeholders with a given index prefix and "_n" suffix.
     *
     * @param string $index
     * @param mixed  $value
     *
     * @return string placeholder
     */
    public function bind($index, $value)
    {
        if (is_int($index)) {
            $index = "_{$index}";
        }

        if (!is_array($value)) {
            $placeholder = "{$index}";

            $this->vars[$placeholder] = $value;

            return ":{$placeholder}";
        }

        $placeholders = [];

        foreach ($value as $subindex => $item) {
            $placeholder = "{$index}_{$subindex}";

            $this->vars[$placeholder] = $item;

            $placeholders[] = ":{$placeholder}";
        }

        return '(' . implode(', ', $placeholders) . ')';
    }

    /**
     * @return array map where variable name => scalar value
     */
    public function getVars()
    {
        return $this->vars;
    }
}
