<?php

namespace mindplay\sql\model;

use mindplay\sql\facets\Mapper;

/**
 * This class implements a simple Mapper that applies a user-defined function to each record.
 */
class RecordMapper implements Mapper
{
    /**
     * @var callable
     */
    private $mapper;

    /**
     * @param callable $mapper function (mixed $record) : mixed
     */
    public function __construct(callable $mapper)
    {
        $this->mapper = $mapper;
    }

    public function map(array $record_set)
    {
        foreach ($record_set as $index => $record) {
            $record_set[$index] = call_user_func($this->mapper, $record);
        }

        return $record_set;
    }
}
