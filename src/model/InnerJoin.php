<?php

namespace mindplay\sql\model;

use mindplay\sql\facets\Expression;

class InnerJoin extends Join
{
    /**
     * @param Node       $node
     * @param Expression $condition
     */
    public function __construct(Node $node, Expression $condition)
    {
        parent::__construct(self::INNER, $node, $condition);
    }
}
