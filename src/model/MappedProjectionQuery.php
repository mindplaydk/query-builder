<?php

namespace mindplay\sql\model;

use mindplay\sql\facets\Mapper;
use mindplay\sql\facets\MapperFactory;
use UnexpectedValueException;

/**
 * This abstract class defines common functionality for Query types involving Mapped Projections.
 */
abstract class MappedProjectionQuery extends ProjectionQuery implements MapperFactory
{
    /**
     * @var Mapper[] list of Mappers to apply to the fetched results
     */
    private $mappers = [];

    /**
     * Append a Mapper instance to apply when each batch of a record-set is fetched.
     *
     * @param Mapper $mapper
     *
     * @return void
     *
     * @see mapRecords() to map an anonymous function against every record
     * @see mapBatches() to map an anonymous function against each batch of records
     */
    public function map(Mapper $mapper)
    {
        $this->mappers[] = $mapper;
    }

    /**
     * Map an anonymous function against every record.
     *
     * @param callable $mapper function (mixed $record) : mixed
     *
     * @return void
     *
     * @see mapBatches() to map an anonymous function against each batch of records
     */
    public function mapRecords(callable $mapper)
    {
        $this->map(new RecordMapper($mapper));
    }

    /**
     * Map an anonymous function against each batch of records.
     *
     * @param callable $mapper function (array $record_set) : array
     *
     * @return void
     *
     * @see mapRecords() to map an anonymous function against every record
     */
    public function mapBatches(callable $mapper)
    {
        $this->map(new BatchMapper($mapper));
    }

    public function createMappers()
    {
        return array_merge(
            [$this->createProjectionMapper()],
            $this->mappers
        );
    }

    /**
     * @return ProjectionMapper
     */
    protected function createProjectionMapper()
    {
        return new ProjectionMapper($this->createProjections());
    }
}
