<?php

namespace mindplay\sql\model;

use mindplay\sql\facets\ProjectionBuilder;
use UnexpectedValueException;

/**
 * This abstract class defines common functionality for Query types involving Projections.
 */
abstract class ProjectionQuery extends Query
{
    /**
     * @var GroupExpression the root Expression of Conditions used in the WHERE clause of a Query
     */
    public $where;

    /**
     * @var Join[] list of JOIN junctions
     */
    public $joins = [];

    /**
     * @var Order[] list of ORDER BY terms
     */
    public $order = [];

    /**
     * @var Node the Root Node of this Query, from which JOIN junctions may occur
     */
    private $root;

    /**
     * @var ProjectionBuilder[]
     */
    private $projection_builders = [];

    /**
     * @param Node $root
     */
    public function __construct(Node $root)
    {
        $this->root = $root;
        $this->where = new GroupExpression('AND');
    }

    /**
     * @return Projection[]
     */
    public function createProjections()
    {
        $projections = [];

        $projection_builders = $this->getProjectionBuilders();

        foreach ($projection_builders as $builder) {
            $projections = array_merge($projections, $builder->buildProjections());
        }

        return $projections;
    }

    /**
     * @return ProjectionBuilder[]
     */
    protected function getProjectionBuilders()
    {
        return $this->projection_builders;
    }

    /**
     * @param ProjectionBuilder|ProjectionBuilder[] one or more Projections to be added
     */
    protected function addProjectionBuilders($projection_builders)
    {
        if (is_array($projection_builders)) {
            $this->projection_builders = array_merge($this->projection_builders, $projection_builders);
        } else {
            $this->projection_builders[] = $projection_builders;
        }
    }

    /**
     * @param Context $context Query Context
     *
     * @return string Root Node and JOIN clauses for use in the FROM clause of an SQL statement
     */
    protected function buildNodes(Context $context)
    {
        $nodes = [$this->root->buildNode($context)];

        foreach ($this->joins as $join) {
            $nodes[] = $join->buildJoin($context);
        }

        return implode("\n", $nodes);
    }

    /**
     * @param Context $context parameter object against which to bind
     * @param string  $index   placeholder index (in some cases used as a prefix for multiple placeholders)
     *
     * @return string conditions for use in the WHERE clause of an SQL statement
     */
    protected function buildConditions(Context $context, $index = '')
    {
        return $this->where->buildExpression($context, $index);
    }

    /**
     * @param Context $context
     *
     * @return string projections for use in a SELECT statement
     *
     * @throws UnexpectedValueException
     */
    protected function buildProjectionVars(Context $context)
    {
        $vars = [];

        $projection_builders = $this->getProjectionBuilders();

        foreach ($projection_builders as $builder) {
            if ($builder instanceof ProjectionBuilder) {
                $vars[] = $builder->buildProjectionVar($context);
            } else {
                throw new UnexpectedValueException("unexpected: " . is_object($builder) ? get_class($builder) : gettype($builder));
            }
        }

        return implode(",\n  ", $vars);
    }

    /**
     * @param Context $context parameter object against which to bind
     * @param string  $index   placeholder index (in some cases used as a prefix for multiple placeholders)
     *
     * @return string terms for use in the ORDER BY clause of an SQL statement
     *
     * @throws UnexpectedValueException if the list of ORDER BY terms is empty
     */
    protected function buildOrderTerms(Context $context, $index = '')
    {
        if (count($this->order) === 0) {
            throw new UnexpectedValueException("unexpected empty order list");
        }

        $order_terms = [];

        foreach ($this->order as $order) {
            $order_terms[] = $order->expression->buildExpression($context, $index)
                . ($order->direction ? " {$order->direction}" : '');
        }

        return implode(', ', $order_terms);
    }
}
