<?php

namespace mindplay\sql\model;

use mindplay\sql\facets\ProjectionBuilder;
use mindplay\sql\schema\Table;

/**
 * This class represents a Table Node, e.g. an alias used to reference a Table in a Query.
 *
 * @see Table::alias()
 */
class Node implements ProjectionBuilder
{
    /**
     * @var Table reference to the Table represented by this Alias
     */
    public $table;

    /**
     * @var string alias name
     */
    public $alias;

    /**
     * @param Table  $table reference to the Table represented by this Alias
     * @param string $alias alias name
     */
    public function __construct(Table $table, $alias)
    {
        $this->table = $table;
        $this->alias = $alias;
    }

    /**
     * Creates a direct Column reference, typically for use as (or in) an Expression, where
     * the column will be reference directly as e.g. `{table node alias}.{column name}`.
     *
     * @param string $column_name
     *
     * @return Variable
     */
    public function column($column_name)
    {
        return new Variable(
            $column_name,
            new QualifiedReference($this->alias, $column_name),
            $this->table->getColumn($column_name)->type
        );
    }

    /**
     * Creates an indirect Column reference, typically for use as (or in) a Projection, where
     * the SQL variable name will be the column name prefixed by the name of this Table Node
     * and an underscore, e.g. `{table node alias}_{column name}`.
     *
     * @param string      $column_name Column name
     * @param string|null $as optional Variable name (overrides the default)
     *
     * @return Variable
     */
    public function project($column_name, $as = null)
    {
        return new Variable(
            $as ?: "{$this->alias}_{$column_name}",
            new QualifiedReference($this->alias, $column_name),
            $this->table->getColumn($column_name)->type
        );
    }

    /**
     * @param Context $context
     *
     * @return string Table Node SQL expression
     */
    public function buildNode(Context $context)
    {
        $alias = $this->table->getName() !== $this->alias
            ? ' AS ' . $context->driver->quoteName($this->alias)
            : ''; // select this Node using the Table name

        return $context->driver->quoteName($this->table->getName()) . $alias;
    }

    public function buildProjectionVar(Context $context)
    {
        return $context->driver->quoteName($this->alias) . '.*';
    }

    public function buildProjections()
    {
        return $this->table->buildProjections();
    }
}
