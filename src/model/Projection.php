<?php

namespace mindplay\sql\model;

use mindplay\sql\facets\Type;

/**
 * This model represents an individual Projection, e.g. a single named value with a known Type.
 */
class Projection
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var Type
     */
    public $type;

    public function __construct($name, Type $type)
    {
        $this->name = $name;
        $this->type = $type;
    }
}
