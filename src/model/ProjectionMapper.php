<?php

namespace mindplay\sql\model;

use mindplay\sql\facets\Converter;
use mindplay\sql\facets\Mapper;
use OutOfBoundsException;
use UnexpectedValueException;

/**
 * This mapper takes a set of Projections and maps a set of rows from a query result to a
 * set of rows with the same structure, but with values converted from SQL to PHP values.
 */
class ProjectionMapper implements Mapper
{
    /**
     * @var Converter[] map where projection name => Converter
     */
    private $converters;

    /**
     * @param Projection[] $projections
     */
    public function __construct($projections)
    {
        $this->converters = $this->createConverters($projections);
    }

    public function map(array $record_set)
    {
        foreach ($record_set as $index => &$record) {
            if (! is_array($record)) {
                throw new UnexpectedValueException("unexpected record type: " . gettype($record));
            }

            foreach ($this->converters as $name => $converter) {
                if (! array_key_exists($name, $record)) {
                    throw new OutOfBoundsException("undefined record field: {$name}");
                }

                $record[$name] = $converter->convertToPHP($record[$name]);
            }
        }

        return $record_set;
    }

    /**
     * @param Projection[] $projections
     *
     * @return Converter[] map where projection name => Converter instance
     */
    protected function createConverters($projections)
    {
        $converters = [];

        foreach ($projections as $projection) {
            if ($projection->type instanceof Converter) {
                $converters[$projection->name] = $projection->type;
            }
        }

        return $converters;
    }
}
