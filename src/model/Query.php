<?php

namespace mindplay\sql\model;

/**
 * Abstract common base class for all types Query types.
 */
abstract class Query
{
    /**
     * @param Context $context parameter object to bind against
     *
     * @return string SQL statement template
     */
    abstract public function buildQuery(Context $context);
}
