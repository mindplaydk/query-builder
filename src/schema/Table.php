<?php

namespace mindplay\sql\schema;

use mindplay\sql\facets\ProjectionBuilder;
use mindplay\sql\model\Context;
use mindplay\sql\model\Node;
use mindplay\sql\model\Projection;
use RuntimeException;

/**
 * This class represents known details about a Table, which is part of a Database.
 *
 * @see Database::getTable()
 */
class Table implements ProjectionBuilder
{
    /**
     * @var string table name
     */
    private $name;

    /**
     * @var Column[] map where Column name => Column instance
     */
    private $columns = [];

    /**
     * @param string   $name table name
     */
    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * @return string table name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Look up a Column definition by name.
     *
     * @param string $name column-name
     *
     * @return Column
     *
     * @throws RuntimeException for undefined Column
     */
    public function getColumn($name)
    {
        if (! isset($this->columns[$name])) {
            throw new RuntimeException("undefined Column: {$name}");
        }

        return $this->columns[$name];
    }

    /**
     * @return Column[] map where Column name => Column instance
     */
    public function listColumns()
    {
        return $this->columns;
    }

    /**
     * @param Column $column
     *
     * @return void
     *
     * @throws RuntimeException for conflicting Column names
     */
    public function registerColumn(Column $column)
    {
        if (isset($this->columns[$column->name])) {
            throw new RuntimeException("conflicting Column registration for name: {$column->name}");
        }

        $this->columns[$column->name] = $column;
    }

    /**
     * Create a Table Node with a given alias, for use in a Query.
     *
     * @param string|null $alias Node alias (or NULL to use the Table name)
     *
     * @return Node
     */
    public function alias($alias)
    {
        return new Node($this, $alias ?: $this->name);
    }

    public function buildProjectionVar(Context $context)
    {
        return $context->driver->quoteName($this->name) . '.*';
    }

    public function buildProjections()
    {
        return array_values(array_map(
            function (Column $column) {
                return new Projection($column->name, $column->type);
            },
            $this->columns
        ));
    }
}
