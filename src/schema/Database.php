<?php

namespace mindplay\sql\schema;

use mindplay\sql\facets\Type;
use mindplay\sql\framework\Driver;
use RuntimeException;

/**
 * This class represents known details about the database, including Tables,
 * Columns and Column Types.
 */
class Database
{
    /**
     * @var Table[] map where table-name => Table instance
     */
    private $tables = [];

    /**
     * @var Type[] map where type-name => Type instance
     */
    private $types = [];

    public function __construct(Driver $driver)
    {
        $driver->bootstrap($this);
    }

    /**
     * Look up a Table definition by name.
     *
     * @param string $name
     *
     * @return Table
     *
     * @throws RuntimeException for undefined Table
     */
    public function getTable($name)
    {
        if (!isset($this->tables[$name])) {
            throw new RuntimeException("undefined Table: {$name}");
        }

        return $this->tables[$name];
    }

    /**
     * @param Table $table
     *
     * @return void
     *
     * @throws RuntimeException for conflicting Table-names
     */
    public function registerTable(Table $table)
    {
        if (isset($this->tables[$table->getName()])) {
            throw new RuntimeException("conflicting Table registration for name: {$table->getName()}");
        }

        $this->tables[$table->getName()] = $table;
    }

    /**
     * Factory-method for quick creation of a basic Table schema with Columns
     *
     * @param string $name
     * @param string[] $columns map where column name => type name
     *
     * @return Table
     */
    public function addTable($name, $columns)
    {
        $table = new Table($name);

        foreach ($columns as $name => $type) {
            $table->registerColumn(new Column($name, $this->getType($type)));
        }

        $this->registerTable($table);

        return $table;
    }

    /**
     * Look up a Type definition by name.
     *
     * @param string $name
     *
     * @return Type
     *
     * @throws RuntimeException for undefined Type
     */
    public function getType($name)
    {
        if (!isset($this->types[$name])) {
            throw new RuntimeException("undefined Type: {$name}");
        }

        return $this->types[$name];
    }

    /**
     * @param Type $type
     *
     * @return void
     *
     * @throws RuntimeException for conflicting Type-names
     */
    public function registerType(Type $type)
    {
        if (isset($this->types[$type->getName()])) {
            throw new RuntimeException("conflicting Type registration for name: {$type->getName()}");
        }

        $this->types[$type->getName()] = $type;
    }
}
