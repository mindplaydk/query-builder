<?php

namespace mindplay\sql\facets;

use mindplay\sql\model\Context;

/**
 * An Expression is any model that can be transformed into a partial SQL statement.
 */
interface Expression
{
    /**
     * @param Context $context parameter object to bind against
     * @param string  $index   placeholder index (in some cases used as a prefix for multiple placeholders)
     *
     * @return string partial SQL statement
     */
    public function buildExpression(Context $context, $index);
}
