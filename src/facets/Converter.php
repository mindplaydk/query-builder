<?php

namespace mindplay\sql\facets;

/**
 * Types may implement this interface for data-types which require some form of conversion
 * between SQL and PHP values.
 */
interface Converter
{
    /**
     * @param mixed $value
     *
     * @return string|int|float|bool|null
     */
    public function convertToSQL($value);

    /**
     * @param string|int|float|null $value
     *
     * @return mixed
     */
    public function convertToPHP($value);
}
