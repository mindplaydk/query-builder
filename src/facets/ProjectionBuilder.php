<?php

namespace mindplay\sql\facets;

use mindplay\sql\model\Context;
use mindplay\sql\model\Projection;

/**
 * A ProjectionBuilder is any model that can build Projections for use in a Query.
 */
interface ProjectionBuilder
{
    /**
     * @param Context $context
     *
     * @return string SQL variable projection expression
     */
    public function buildProjectionVar(Context $context);

    /**
     * @return Projection[] list of Projections
     */
    public function buildProjections();
}
