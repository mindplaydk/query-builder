<?php

namespace mindplay\sql\facets;

/**
 * This interface defines the responsibilities of a Type definition.
 *
 * @see Database::getType()
 */
interface Type
{
    /**
     * @return string
     */
    public function getName();
}
