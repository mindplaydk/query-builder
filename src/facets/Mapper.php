<?php

namespace mindplay\sql\facets;

/**
 * This interface defines a means of mapping a set of record to a different set of records or objects.
 *
 * The two primary use-cases for a Mapper, is the built-in ProjectionMapper, and your user-defined
 * domain-specific Mappers which may for example map to domain objects.
 */
interface Mapper
{
    /**
     * @param array $record_set
     *
     * @return array
     */
    public function map(array $record_set);
}
