<?php

namespace mindplay\sql\facets;

use mindplay\sql\model\Context;

/**
 * This interfaces defines a Reference as something that requires a Context to be resolved.
 */
interface Reference
{
    /**
     * @param Context $context
     *
     * @return string literal reference
     */
    public function resolve(Context $context);
}
