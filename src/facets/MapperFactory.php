<?php

namespace mindplay\sql\facets;

/**
 * Query types (such as {@see SelectQuery}) which provide a list of Mappers implement this interface.
 */
interface MapperFactory
{
    /**
     * @return Mapper[] list of Mappers to be be applied
     */
    public function createMappers();
}
