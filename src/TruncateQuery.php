<?php

namespace mindplay\sql;

use mindplay\sql\model\Context;
use mindplay\sql\model\Query;
use mindplay\sql\schema\Table;

class TruncateQuery extends Query
{
    /**
     * @var Table
     */
    public $table;

    public function __construct(Table $table)
    {
        $this->table = $table;
    }

    public function buildQuery(Context $context)
    {
        $table = $context->driver->quoteName($this->table);

        return "TRUNCATE TABLE {$table}";
    }
}
