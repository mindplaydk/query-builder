<?php

namespace mindplay\sql\drivers;

use mindplay\sql\framework\Driver;
use mindplay\sql\schema\Database;
use mindplay\sql\types\IntType;
use mindplay\sql\types\JSONType;
use mindplay\sql\types\StringType;
use mindplay\sql\types\TimestampType;

class MySQLDriver extends Driver
{
    public function quoteName($name)
    {
        return '`' . $name . '`';
    }

    public function bootstrap(Database $db)
    {
        $db->registerType(new StringType());
        $db->registerType(new IntType());
        $db->registerType(new TimestampType());
        $db->registerType(new JSONType());
    }
}
