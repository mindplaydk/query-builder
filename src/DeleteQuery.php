<?php

namespace mindplay\sql;

use mindplay\sql\model\Context;
use mindplay\sql\model\ProjectionQuery;
use mindplay\sql\schema\Table;

/**
 * This class represents a DELETE query.
 */
class DeleteQuery extends ProjectionQuery
{
    /**
     * @var Table
     */
    private $table;

    /**
     * @param Table $from Table to DELETE from
     */
    public function __construct(Table $from)
    {
        parent::__construct($from->alias(null));

        $this->table = $from;
    }

    public function buildQuery(Context $context)
    {
        $delete = "DELETE FROM " . $this->buildNodes($context);

        $where = count($this->where->conds)
            ? "\nWHERE " . $this->buildConditions($context)
            : ''; // no conditions present

        return "{$delete}{$where}";
    }
}
