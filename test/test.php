<?php

require __DIR__ . '/header.php';

use mindplay\sql\DeleteQuery;
use mindplay\sql\drivers\MySQLDriver;
use mindplay\sql\framework\Connection;
use mindplay\sql\framework\Driver;
use mindplay\sql\InsertQuery;
use mindplay\sql\model\Comparison;
use mindplay\sql\model\Context;
use mindplay\sql\model\GroupExpression;
use mindplay\sql\model\Order;
use mindplay\sql\model\Projection;
use mindplay\sql\model\RightJoin;
use mindplay\sql\schema\Column;
use mindplay\sql\schema\Database;
use mindplay\sql\SelectQuery;
use mindplay\sql\types\JSONType;
use mindplay\sql\types\TimestampType;
use mindplay\sql\UpdateQuery;
use Mockery\MockInterface;

configure()->enableCodeCoverage(__DIR__ . '/build/clover.xml', dirname(__DIR__) . '/src');

/**
 * @return Driver
 */
function create_driver()
{
    return new MySQLDriver();
}

/**
 * @param Driver $driver
 *
 * @return Database
 */
function create_db(Driver $driver)
{
    $db = new Database($driver);

    $db->addTable('users', [
        'first_name' => 'string',
        'last_name'  => 'string',
        'login'      => 'string',
        'alt_email'  => 'string',
        'group_id'   => 'int',
        'country_id' => 'int',
    ]);

    $db->addTable('countries', [
        'id'   => 'int',
        'name' => 'string',
    ]);

    $db->getTable('countries')->getColumn('id')->auto();

    return $db;
}

/**
 * @param Projection[] $projections
 * @param string[] $expected_types map where projection name => type name
 */
function check_projection_types($projections, $expected_types)
{
    $num_types = count($expected_types);
    $num_projections = count($projections);

    if ($num_types !== $num_projections) {
        ok(false, "projection count mismatch - expected: {$num_types}, got: {$num_projections}");
    }

    /** @var Projection[] $actual_projections map where Projection name => Projection */
    $actual_projections = [];

    foreach ($projections as $projection) {
        $actual_projections[$projection->name] = $projection;
    }

    foreach ($expected_types as $projection_name => $expected_type) {
        if (isset($actual_projections[$projection_name])) {
            $projection = $actual_projections[$projection_name];

            eq(
                $projection->type->getName(),
                $expected_type,
                "projection '{$projection->name}' should have type: {$expected_type}"
            );
        } else {
            ok(false, "projection '{$projection_name}' does not exist");
        }
    }
}

test(
    'Table/Column behavior',
    function () {
    }
);

test(
    "can build complex query",
    function () {
        $driver = create_driver();
        $db = create_db($driver);

        $user = $db->getTable('users')->alias('user');
        $country = $db->getTable('countries')->alias('country');

        $query = new SelectQuery($user);
        $query->joins[] = new RightJoin($country,
            new Comparison($user->column('country_id'), '=', $country->column('id')));

        $query->select([$user, $country->project('name')]);

        $query->where->conds['first_name'] = new Comparison($user->column('first_name'), 'LIKE', '%rasmus%');
        $query->where->conds['last_name'] = new Comparison($user->column('last_name'), 'LIKE', '%schultz%');
        $query->where->conds['email'] = $email = new GroupExpression('OR');
        $email->conds['login'] = new Comparison($user->column('login'), 'LIKE', '%rasc%');
        $email->conds['alt'] = new Comparison($user->column('alt_email'), 'LIKE', '%rasc%');
        $query->where->conds['group'] = new Comparison($user->column('group_id'), 'IN', [1, 2, 3]);
        $query->where->conds['country'] = new Comparison($country->column('name'), '=', 'Denmark');

        $query->order[] = new Order($user->column('first_name'), Order::ASC);
        $query->order[] = new Order($user->column('last_name'), Order::ASC);

        $query->page(2, 20);

        $expected = <<<SQL
SELECT `user`.*,
  `country`.`name` AS `country_name`
FROM `users` AS `user`
RIGHT JOIN `countries` AS `country` ON `user`.`country_id` = `country`.`id`
WHERE (`user`.`first_name` LIKE :first_name AND `user`.`last_name` LIKE :last_name AND (`user`.`login` LIKE :email_login OR `user`.`alt_email` LIKE :email_alt) AND `user`.`group_id` IN (:group_0, :group_1, :group_2) AND `country`.`name` = :country)
ORDER BY `user`.`first_name` ASC, `user`.`last_name` ASC
LIMIT 20 OFFSET 20
SQL;

        $context = new Context($driver);

        eq($query->buildQuery($context), $expected);

        eq($context->params->getVars(), [
            'first_name'  => '%rasmus%',
            'last_name'   => '%schultz%',
            'email_login' => '%rasc%',
            'email_alt'   => '%rasc%',
            'group_0'     => 1,
            'group_1'     => 2,
            'group_2'     => 3,
            'country'     => 'Denmark',
        ]);

        check_projection_types($query->createProjections(), [
            "first_name"   => "string",
            "last_name"    => "string",
            "login"        => "string",
            "alt_email"    => "string",
            "group_id"     => "int",
            "country_id"   => "int",
            "country_name" => "string",
        ]);
    }
);

test(
    "can build nested query",
    function () {
        $driver = create_driver();
        $db = create_db($driver);

        $user = $db->getTable('users')->alias('user');
        $country = $db->getTable('countries')->alias('country');

        $query = new SelectQuery($user);
        $query->select($user);

        $some_countries = new SelectQuery($country);
        $some_countries->select($country->project("id"));
        $some_countries->where->conds["country_name"] = new Comparison($country->column("name"), "LIKE", "%den%");

        $query->where->conds[] = new Comparison($user->column("country_id"), "IN", $some_countries);

        $expected = <<<SQL
SELECT `user`.*
FROM `users` AS `user`
WHERE `user`.`country_id` IN (SELECT `country`.`id` AS `country_id`
FROM `countries` AS `country`
WHERE `country`.`name` LIKE :country_name)
SQL;

        $context = new Context($driver);

        eq($query->buildQuery($context), $expected);
        eq($context->params->getVars(), ["country_name" => "%den%"]);

        check_projection_types($query->createProjections(), [
            "first_name"   => "string",
            "last_name"    => "string",
            "login"        => "string",
            "alt_email"    => "string",
            "group_id"     => "int",
            "country_id"   => "int",
        ]);
    }
);

test(
    'can build INSERT query',
    function () {
        $driver = create_driver();
        $db = create_db($driver);

        $countries = $db->getTable('countries');

        // we need an extra column to check the formatting of column-names and tuples in this case:
        $countries->registerColumn(new Column('currency', $db->getType('string')));

        $query = new InsertQuery($countries, ['name' => 'Denmark', 'currency' => 'DKK']);

        $expected_sql = <<<'SQL'
INSERT INTO `countries` (`name`, `currency`) VALUES
(:_0, :_1)
SQL;

        $context = new Context($driver);

        eq($query->buildQuery($context), $expected_sql, "can insert single record");

        eq($context->params->getVars(), [
            '_0' => 'Denmark',
            '_1' => 'DKK',
        ]);
    }
);

test(
    'can build multiple-INSERT query',
    function () {
        $driver = create_driver();
        $db = create_db($driver);

        $countries = $db->getTable('countries');

        $query = new InsertQuery($countries, [
            ['name' => 'Denmark'],
            ['name' => 'Norway'],
            ['name' => 'Sweden'],
        ]);

        $expected_sql = <<<'SQL'
INSERT INTO `countries` (`name`) VALUES
(:_0),
(:_1),
(:_2)
SQL;

        $context = new Context($driver);

        eq($query->buildQuery($context), $expected_sql, "can insert multiple records");

        eq($context->params->getVars(), [
            '_0'  => 'Denmark',
            '_1'  => 'Norway',
            '_2'  => 'Sweden',
        ]);
    }
);

test(
    'can build DELETE query',
    function () {
        $driver = create_driver();
        $db = create_db($driver);

        $countries = $db->getTable('countries');

        $query = new DeleteQuery($countries);

        $query->where->conds['country_name'] = new Comparison($countries->getColumn('name'), '=', 'Germany'); // (nothing personal)

        $context = new Context($driver);

        $expected_sql = <<<'SQL'
DELETE FROM `countries`
WHERE `name` = :country_name
SQL;

        eq($query->buildQuery($context), $expected_sql, "can create DELETE query");

        eq($context->params->getVars(), [
            'country_name'  => 'Germany',
        ]);
    }
);

test(
    'can build UPDATE query',
    function () {
        $driver = create_driver();
        $db = create_db($driver);

        $users = $db->getTable('users');

        $query = new UpdateQuery($users);

        $query->where->conds['group_id'] = new Comparison($users->getColumn('group_id'), '=', 1);

        $query->set('group_id', 2);

        $context = new Context($driver);

        $expected_sql = <<<'SQL'
UPDATE `users`
SET `group_id` = :new_group_id
WHERE `group_id` = :group_id
SQL;

        eq($query->buildQuery($context), $expected_sql, "can create DELETE query");

        eq($context->params->getVars(), [
            'new_group_id' => 2,
            'group_id' => 1,
        ]);
    }
);

test(
    'can build COUNT expression',
    function () {
        // TODO
    }
);

test(
    'can apply Mappers',
    function () {
        $driver = create_driver();
        $db = create_db($driver);

        /** @var MockInterface|PDO $mock_pdo */
        $mock_pdo = Mockery::mock(PDO::class);

        /** @var MockInterface|PDOStatement $mock_statement */
        $mock_statement = Mockery::mock(PDOStatement::class);

        $mock_pdo
            ->shouldReceive('prepare')
            ->once()
            ->andReturn($mock_statement);

        $mock_statement
            ->shouldReceive('execute')
            ->once()
            ->andReturn(true);

        $mock_statement
            ->shouldReceive('fetch')
            ->once()
            ->andReturn(['id' => '123', 'name' => 'Denmark']);

        $mock_statement
            ->shouldReceive('fetch')
            ->once()
            ->andReturn(false);

        $connection = new Connection($driver, $mock_pdo);

        $country = $db->getTable('countries')->alias('country');

        $query = new SelectQuery($country);

        $query->select($country);

        $query->mapRecords(function ($record) {
            $record['mapped'] = true;

            return $record;
        });

        $result = iterator_to_array($connection->fetch($query));

        eq(
            $result,
            [
                ['id' => 123, 'name' => 'Denmark', 'mapped' => true],
            ],
            "it should cast the 'id' column to int"
        );

        Mockery::close();
    }
);

test(
    'transaction() commits when function returns true',
    function () {
        /** @var MockInterface|PDO $mock_pdo */
        $mock_pdo = Mockery::mock(PDO::class);
        $driver = create_driver();
        $connection = new Connection($driver, $mock_pdo);

        $mock_pdo->shouldReceive('beginTransaction')->once();
        $mock_pdo->shouldReceive('commit')->once();

        $result = $connection->transact(function () {
            return true;
        });

        Mockery::close();

        eq($result, true, "transaction succeeds");
    }
);

test(
    'transaction() commits when nested transactions succeed',
    function () {
        /** @var MockInterface|PDO $mock_pdo */
        $mock_pdo = Mockery::mock(PDO::class);
        $driver = create_driver();
        $connection = new Connection($driver, $mock_pdo);

        $mock_pdo->shouldReceive('beginTransaction')->once();
        $mock_pdo->shouldReceive('commit')->once();

        $result = $connection->transact(function () use ($connection) {
            $connection->transact(function () {
                return true; // inner transcation succeeds
            });

            return true; // outer transaction succeeds
        });

        Mockery::close();

        eq($result, true, "transaction succeeds");
    }
);

test(
    'transact() rolls back when function returns false',
    function () {
        /** @var MockInterface|PDO $mock_pdo */
        $mock_pdo = Mockery::mock(PDO::class);
        $driver = create_driver();
        $connection = new Connection($driver, $mock_pdo);

        $mock_pdo->shouldReceive('beginTransaction')->once();
        $mock_pdo->shouldReceive('rollBack')->once();

        $result = $connection->transact(function () {
            return false;
        });

        Mockery::close();

        eq($result, false, "transaction fails");
    }
);

test(
    'transact() rolls back when function returns void',
    function () {
        /** @var MockInterface|PDO $mock_pdo */
        $mock_pdo = Mockery::mock(PDO::class);
        $driver = create_driver();
        $connection = new Connection($driver, $mock_pdo);

        $mock_pdo->shouldReceive('beginTransaction')->once();
        $mock_pdo->shouldReceive('rollBack')->once();

        expect(
            UnexpectedValueException::class,
            "should throw when function fails to indicate success/failure",
            function () use ($connection) {
                $connection->transact(function () {
                    // return void
                });
            }
        );

        Mockery::close();

        ok(true, "transcation fails");
    }
);

test(
    'transact() rolls back when function throws an exception',
    function () {
        /** @var MockInterface|PDO $mock_pdo */
        $mock_pdo = Mockery::mock(PDO::class);
        $driver = create_driver();
        $connection = new Connection($driver, $mock_pdo);

        $mock_pdo->shouldReceive('beginTransaction')->once();
        $mock_pdo->shouldReceive('rollBack')->once();

        expect(
            LogicException::class,
            "should throw when function throws an exception",
            function () use ($connection) {
                $connection->transact(function () {
                    throw new RuntimeException("oops!");
                });
            }
        );

        Mockery::close();

        ok(true, "transaction fails");
    }
);

test(
    'transact() rolls back when a nested call to transact() fails',
    function () {
        /** @var MockInterface|PDO $mock_pdo */
        $mock_pdo = Mockery::mock(PDO::class);
        $driver = create_driver();
        $connection = new Connection($driver, $mock_pdo);

        $mock_pdo->shouldReceive('beginTransaction')->once();
        $mock_pdo->shouldReceive('rollBack')->once();

        $result = $connection->transact(function () use ($connection) {
            $connection->transact(function () {
                return false; // inner function fails
            });

            return true; // outer transaction succeeds
        });

        Mockery::close();

        eq($result, false, "transaction fails");
    }
);

test(
    'transact() rolls back when one of several nested calls to transact() fails',
    function () {
        /** @var MockInterface|PDO $mock_pdo */
        $mock_pdo = Mockery::mock(PDO::class);
        $driver = create_driver();
        $connection = new Connection($driver, $mock_pdo);

        $mock_pdo->shouldReceive('beginTransaction')->once();
        $mock_pdo->shouldReceive('rollBack')->once();

        $result = $connection->transact(function () use ($connection) {
            $connection->transact(function () {
                return true; // first inner function succeeds
            });

            $connection->transact(function () {
                return false; // second inner function fails
            });

            $connection->transact(function () {
                return true; // third inner function succeeds
            });

            return true; // outer transaction succeeds
        });

        Mockery::close();

        eq($result, false, "transaction fails");
    }
);

test(
    'can map DATETIME to Unix timestamps',
    function () {
        $converter = new TimestampType();

        $valid_datetime = '2015-11-04 14:40:52';
        $valid_timestamp = 1446648052;

        eq($converter->convertToPHP($valid_datetime), $valid_timestamp, "can convert to PHP value");
        eq($converter->convertToSQL($valid_timestamp), $valid_datetime, "can convert to SQL DATETIME value");
    }
);

test(
    'can map PHP values to JSON',
    function () {
        $converter = new JSONType();

        $valid_value = ['foo' => 'bar'];
        $valid_json = '{"foo":"bar"}';

        eq($converter->convertToPHP($valid_json), $valid_value, "can convert to PHP value");
        eq($converter->convertToSQL($valid_value), $valid_json, "can convert to SQL JSON value");
    }
);

test(
    'can fetch records and apply Mappers in batches',
    function () {
        $driver = create_driver();
        $db = create_db($driver);

        /** @var MockInterface|PDO $mock_pdo */
        $mock_pdo = Mockery::mock(PDO::class);

        /** @var MockInterface|PDOStatement $mock_statement */
        $mock_statement = Mockery::mock(PDOStatement::class);

        $mock_pdo
            ->shouldReceive('prepare')
            ->once()
            ->andReturn($mock_statement);

        $mock_statement
            ->shouldReceive('execute')
            ->once()
            ->andReturn(true);

        $mock_statement
            ->shouldReceive('fetch')
            ->times(30)
            ->andReturnValues(array_map(function ($id) { return ['id' => $id, 'name' => 'Denmark']; }, range(1,30)));

        $mock_statement
            ->shouldReceive('fetch')
            ->once()
            ->andReturn(false);

        $connection = new Connection($driver, $mock_pdo);

        $country = $db->getTable('countries')->alias('country');

        $query = new SelectQuery($country);

        $query->select($country);

        $query->mapRecords(function (array $record) {
            $record['mapped'] = true;

            return $record;
        });

        $batch_num = 0;

        $query->mapBatches(function (array $records) use (&$batch_num) {
            $batch_num += 1;

            foreach ($records as &$record) {
                $record['batch_num'] = $batch_num;
            }

            return $records;
        });

        foreach ($connection->fetch($query, 20) as $index => $record) {
            eq($record['id'], $index + 1);
            eq($record['batch_num'], (int) floor($index / 20) + 1, $record['batch_num']);
            ok($record['mapped']);
        }

        Mockery::close();
    }
);

exit(run());
