mindplay/sql
============

This library implements an SQL query abstraction.

One of my coworkers described it as a kind of [AST](https://en.wikipedia.org/wiki/Abstract_syntax_tree)
for SQL, which I think is a good description covering probably 80% of what this library is.

The concept is different from that of most libraries describing themselves as "query builders", in that
most of them build parts of the query as strings as you go along, and glue them together at the end -
a stated objective for this library, is to build nothing on the fly; rather, the entire query is built
from a model, on-demand. The goal of this objective, is to leave the entire model open to modification
and introspection, up until the moment when you're ready to build and execute the query - for example,
table/column names do not get quoted, and expressions are represented as objects rather than SQL snippets.

Another key difference is the criteria model, which consists of expression trees, rather than a list of
strings. You will find that working with this model is more verbose than working with the fluent APIs of
some other query builders - for example, you won't find methods with names like `andWhere()`, but instead
a model object representing a group expression as list of expressions to be combined with `AND` or `OR`.
(This is where some of the AST-like feel stems from.)

The library is divided into a number of namespaces:

```
mindplay\sql      The root namespace with top-level models (SelectQuery, UpdateQuery, etc.)
  facets          Interface definitions outlining various concepts and responsibilities
  model           The query model itself (representing JOINs, projections, expressions, etc.)
  schema          Schema abstractions, including `Database`, `Table` and `Column` models
  framework       Framework components (such as Connection and Statement) to support PDO
  drivers         Drivers implementing details specific to various DBMS back-ends
  types           Data-type definitions, performs conversion between PHP and SQL types, etc.
```

